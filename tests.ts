import * as mongoose from 'mongoose';

const modelName = 'order_tests';

const orderSchema = new mongoose.Schema({
    order_id: { type: String, required: true },    // this is  system generated. Comes from order collection
    patient_id: { type: String, required: true },  // this is system generated id which comes from patient collection
    test_info: {
                    test_id: { type: String, required: true, trim: true },
                    test_code: { type: String, required: true, trim: true },
                    name: { type: String, trim: true },
                    is_supplementary: { type: Boolean, required: true },
                    currency: { type: String, default: "IND", enum: ['IND', 'USA']},
                    unit_price: { type: Number, required: true },
                    discount_percentage: { type: Number },
                    discount_amount: { type: Number },
                    actual_amount: { type: Number, required: true },
                    documents: [{
                                    id: { type: String },
                                    name: { type: String },
                                    path: { type: String }
                                }]
                },
    is_active: { type: Boolean, default: true },
    created_date: { type: Date, default: Date.now },
    modified_date: { type: Date, default: Date.now }
});

const test = mongoose.model(modelName, orderSchema);
 
export { test };
