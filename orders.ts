import * as mongoose from 'mongoose';

const modelName = 'orders';

const orderSchema = new mongoose.Schema({
    batch_id: { type: String, required: true },    // this is system generated id which comes from batch collection 
    patient_id: { type: String, required: true },  // this is system generated id which comes from patient collection 
    order_unique_id: { type: String, required: true },    // this is not system generated. Its unique for every order
    reffered_by : { 
                    type: String, 
                    required: true,
                    enum: ['self', 'doctor'] 
                },
    address: { 
                street: { type: String, required: true },
                city: { type: String },
                state: { type: String },
                country: { type: String },
                pincode: { type: Number }
            },
    doctor_info: {
                    ultra_code: { type: String },
                    reg_number: { type: String },
                    name: { type: String },
                    specialization: { type: String },
                    hospital: { type: String },
                    city: { type: String },
                },
    patient_type: { type: String, required: true },
    lab_number: { type: Number, required: true },
    invoice_account: { type: Number, required: true },
    processing_lab: { type: String, required: true },
    collection_datetime : { type: Date, required: true },
    is_rsr: { type: Boolean, required: true },
    rsr_info: {
                reason: { type: String },
                ref_reg_number: { type: String },
                previous_lab_number: { type: String }
            },
    subscription_info: {
                        card_type: { type: String },
                        wellness_card: { type: String },
                        dcp_card: { type: String },
                      },
    promo_code: { type: String },
    actual_amount: { type: Number,  default: 0 },
    amount_after_discount: { type: Number,  default: 0 },
    source: { type: String, required: true },
    status: { type: Boolean, required: true },
    created_date: { type: Date, default: Date.now },
    modified_date: { type: Date, default: Date.now }
});

const order = mongoose.model(modelName, orderSchema);
 
export { order };
